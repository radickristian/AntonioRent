$(document).ready(function() {
	$('.slider').slick({
  		fade: true,
  		dots: true,
  		autoplay: true,
  		arrows: true
	});

	$('.slide').slick({
  		fade: true,
  		autoplay: true,
  		arrows: true
	});

	$("ul.nav-tabs a").click(function (e) {
	  e.preventDefault();  
	    $(this).tab('show').fadeIn(1000);
	});

	$('.offcanvas-toggle').on('click', function() {
	  $('body').toggleClass('offcanvas-expanded');
	});

	$(window).scroll(function () { 
	    console.log($(window).scrollTop());
	    if ($(window).scrollTop() > 150) {
	      $('.navigation').addClass('navbar-fixed-top');
	    }

	    if ($(window).scrollTop() < 150) {
	      $('.navigation').removeClass('navbar-fixed-top');
	    }
  	});

  	$('.map').click(function () {
    	$('.map iframe').css("pointer-events", "auto");
	});

	$('.main-nav__item a[href^="/#"], .mobile-nav__item a[href^="/#"]')
		.each(function(index, item){
			var $anchor = $($(item).attr('href').replace('/#!', '#'));
			if ($anchor.length > 0) {
				$(item).attr('href', $(item).attr('href').replace('/#!', '#'));
			}
		});

	$('#test').on('click', function() {
    	$('body').removeClass('offcanvas-expanded');
	});


});


